'use strict';

var io = require('socket.io-client');
var dJSON = require('dirty-json');

var stdout = process.stdout;
var stderr = process.stderr;

var PROTO_TYPE;

// REDIS variables
var nrp;

// SOCKETIO variables
var socketClient;
var logServerUrl;
var authToken;
var logBuffer = [];
var maxLogBufferSize = 500;

/**
 * [install_hook_to description]
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
var install_hook_to = function(obj) {

    if (obj.hook || obj.unhook) {
        throw new Error('Object already has properties hook and/or unhook');
    }

    obj.hook = function(_meth_name, _fn, _is_async) {
        var self = this,
            meth_ref;

        // Make sure method exists
        if (!(Object.prototype.toString.call(self[_meth_name]) === '[object Function]')) {
            throw new Error('Invalid method: ' + _meth_name);
        }

        // We should not hook a hook
        if (self.unhook.methods[_meth_name]) {
            throw new Error('Method already hooked: ' + _meth_name);
        }

        // Reference default method
        meth_ref = (self.unhook.methods[_meth_name] = self[_meth_name]);

        self[_meth_name] = function() {
            var args = Array.prototype.slice.call(arguments);

            // Our hook should take the same number of arguments 
            // as the original method so we must fill with undefined
            // optional args not provided in the call
            while (args.length < meth_ref.length) {
                args.push(undefined);
            }

            // Last argument is always original method call
            args.push(function() {
                var args = arguments;

                if (_is_async) {
                    process.nextTick(function() {
                        meth_ref.apply(self, args);
                    });
                } else {
                    meth_ref.apply(self, args);
                }
            });

            _fn.apply(self, args);
        };
    };

    obj.unhook = function(_meth_name) {
        var self = this,
            ref = self.unhook.methods[_meth_name];

        if (ref) {
            self[_meth_name] = self.unhook.methods[_meth_name];
            delete self.unhook.methods[_meth_name];
        } else {
            throw new Error('Method not hooked: ' + _meth_name);
        }
    };

    obj.unhook.methods = {};
};

/**
 * [_socketioConnect description]
 * @return {[type]} [description]
 */
function _socketioConnect() {
    socketClient = io(logServerUrl, {
        extraHeaders: {
            Authorization: authToken
        }
    });

    socketClient.on('connect', function() {
        console.log("SocketIO error loggregator connected");
        // Is there a buffer that needs to be processed first?
        // If yes, send buffered logs before continuing
        if (logBuffer.length > 0) {
            for (var i = 0; i < logBuffer.length; i++) {
                socketClient.emit(logBuffer[i].type, logBuffer[i].msg);
                logBuffer.splice(i, 1);
                i--;
            }
        }
    });
    socketClient.on('disconnect', function() {
        console.log("SocketIO error loggregator disconnected");
    });
}

/**
 * [initProtocol description]
 * @param  {[type]} type [description]
 * @param  {[type]} auth [description]
 * @return {[type]}      [description]
 */
module.exports.initProtocol = function(type, auth) {
    PROTO_TYPE = type;
    if (type === "SOCKETIO") {
        logServerUrl = auth.logregatorURL;
        authToken = auth.authorizationToken;

        _socketioConnect();
    } else if (type === "REDIS") {
        var NRP = require('node-redis-pubsub');
        nrp = new NRP(auth);
    }
};

/**
 * [startForward description]
 * @return {[type]} [description]
 */
module.exports.startForward = function() {
    if (!stdout.hook) {
        install_hook_to(stdout);
        install_hook_to(stderr);
    }

    /**
     * console.log output, look for error keyword
     * @param  {[type]} string   [description]
     * @param  {[type]} encoding [description]
     * @param  {[type]} fd       [description]
     * @param  {[type]} write    [description]
     * @return {[type]}          [description]
     */
    function _interceptLog(string, encoding, fd, write) {

        string = string.trim().replace(/^\s+|\s+$/g, ''); // Remove carriage return at end of line
        if (string.indexOf('{') === 0 || string.indexOf('[') === 0) {
            dJSON.parse(string).then(function(jsonString) {
                // If JSON object is of type error
                if (jsonString && jsonString.type && jsonString.type === 'error') {
                    if (PROTO_TYPE === "SOCKETIO") {
                        if (socketClient && socketClient.id) {
                            socketClient.emit('error_msg', string);
                        } else {
                            if (logBuffer.length > maxLogBufferSize) {
                                logBuffer.shift();
                            }
                            logBuffer.push({
                                type: "error_msg",
                                msg: string
                            });
                        }
                    } else if (PROTO_TYPE === "REDIS") {
                        nrp.emit('error_msg', string);
                    }
                }
            });
        } else {
            var _logMsg = string.toLowerCase();
            // If log contains error keyword
            if (_logMsg.indexOf('error') != -1) {
                if (PROTO_TYPE === "SOCKETIO") {
                    if (socketClient && socketClient.id) {
                        socketClient.emit('error_msg', string);
                    } else {
                        if (logBuffer.length > maxLogBufferSize) {
                            logBuffer.shift();
                        }
                        logBuffer.push({
                            type: "error_msg",
                            msg: string
                        });
                    }
                } else if (PROTO_TYPE === "REDIS") {
                    nrp.emit('error_msg', string);
                }
            }
        }

        write(string + '\n');
    }

    /**
     * console.err output, log it no mater what's inside
     * @param  {[type]} string   [description]
     * @param  {[type]} encoding [description]
     * @param  {[type]} fd       [description]
     * @param  {[type]} write    [description]
     * @return {[type]}          [description]
     */
    function _interceptErr(string, encoding, fd, write) {
        string = string.trim().replace(/^\s+|\s+$/g, ''); // Remove carriage return at end of line
        if (PROTO_TYPE === "SOCKETIO") {
            if (socketClient && socketClient.id) {
                socketClient.emit('error_msg', string);
            } else {
                if (logBuffer.length > maxLogBufferSize) {
                    logBuffer.shift();
                }
                logBuffer.push({
                    type: "error_msg",
                    msg: string
                });
            }
        } else if (PROTO_TYPE === "REDIS") {
            nrp.emit('error_msg', string);
        }
        write(string + '\n');
    }

    stdout.hook('write', _interceptLog, true);
    stderr.hook('write', _interceptErr, true);
};

/**
 * [unhook description]
 * @return {[type]} [description]
 */
module.exports.stopForward = function() {
    stdout.unhook('write');
    stderr.unhook('write');
    logServerUrl = null;
};

/**
 * [description]
 * @return {[type]}      [description]
 */
process.on('uncaughtException', function(err) {
    console.error(err.stack);
    // Since the logging upload is async, we need to wait a bit 
    // before exiting to give it time to upload the info to the LogHub
    setTimeout(function() {
        process.exit(1);
    }, 1000);
});